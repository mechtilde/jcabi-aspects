Source: jcabi-aspects
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Mechtilde Stehmann <mechtilde@debian.org>
Build-Depends: debhelper-compat (= 13), default-jdk, maven-debian-helper,
 libjcabi-log-java, libaspectj-java, libaspectj-maven-plugin-java,
 libtemplating-maven-plugin-java, libmaven-bundle-plugin-java,
 libgeronimo-validation-1.1-spec-java
Standards-Version: 4.5.1
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/java-team/jcabi-aspects.git
Vcs-Browser: https://salsa.debian.org/java-team/jcabi-aspects
Homepage: http://aspects.jcabi.com/

Package: libjcabi-aspects-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Description: Collection of AOP/AspectJ Java Aspects
 This module contains a collection of useful AOP aspects, which allow you
 to modify the behavior of a Java application without writing a line of
 code.
 .
 For example, you may want to retry HTTP resource downloading in case of
 failure. You can implement a full do/while cycle yourself, or you can
 annotate your method with @RetryOnFailure and let one of our AOP
 aspects do the work for you.
